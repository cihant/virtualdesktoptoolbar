﻿using CSDeskBand;
using CSDeskBand.Annotations;
using CSDeskBand.ContextMenu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using WindowsDesktop;

namespace VirtualDesktopToolbar
{
    /// <summary>
    /// Example WPF deskband. Shows taskbar info capabilities and context menus
    /// </summary>
    [ComVisible(true)]
    [Guid("89BF6B36-A0B0-4C95-A666-87A55C226986")]
    [CSDeskBandRegistration(Name = "Virtual Desktop Toolbar", ShowDeskBand = true)]
    public partial class UserControl1 : INotifyPropertyChanged
    {
        private Orientation _taskbarOrientation;
        private int _taskbarWidth;
        private int _taskbarHeight;
        private Edge _taskbarEdge;
        private int rowCount = 4;
        public Orientation TaskbarOrientation
        {
            get => _taskbarOrientation;
            set
            {
                if (value == _taskbarOrientation) return;
                _taskbarOrientation = value;
                OnPropertyChanged();
            }
        }

        public int TaskbarWidth
        {
            get => _taskbarWidth;
            set
            {
                if (value == _taskbarWidth) return;
                _taskbarWidth = value;
                OnPropertyChanged();
            }
        }

        public int TaskbarHeight
        {
            get => _taskbarHeight;
            set
            {
                if (value == _taskbarHeight) return;
                _taskbarHeight = value;
                OnPropertyChanged();
            }
        }

        public Edge TaskbarEdge
        {
            get => _taskbarEdge;
            set
            {
                if (value == _taskbarEdge) return;
                _taskbarEdge = value;
                OnPropertyChanged();
            }
        }

        //private List<DeskBandMenuItem> ContextMenuItems
        //{
        //    get
        //    {
        //        var action = new DeskBandMenuAction("Action - Toggle submenu");
        //        var separator = new DeskBandMenuSeparator();
        //        var submenuAction = new DeskBandMenuAction("Submenu Action - Toggle checkmark");
        //        var submenu = new DeskBandMenu("Submenu")
        //        {
        //            Items = { submenuAction }
        //        };

        //        action.Clicked += (sender, args) => submenu.Enabled = !submenu.Enabled;
        //        submenuAction.Clicked += (sender, args) => submenuAction.Checked = !submenuAction.Checked;

        //        return new List<DeskBandMenuItem>() { action, separator, submenu };
        //    }
        //}

        private Guid _currentDesktopId;

        public Guid CurrentDesktopId
        {
            get { return _currentDesktopId; }
            set
            {
                if (value == _currentDesktopId) return;
                _currentDesktopId = value;
                OnPropertyChanged();
            }
        }

        public UserControl1()
        {
            InitializeComponent();
            //Options.MinHorizontalSize.Width = 500;
            //Options.MinVerticalSize.Width = 130;
            //Options.MinVerticalSize.Height = 200;

            TaskbarInfo.TaskbarEdgeChanged += (sender, args) => TaskbarEdge = args.Edge;
            TaskbarInfo.TaskbarOrientationChanged += (sender, args) => TaskbarOrientation = args.Orientation == CSDeskBand.TaskbarOrientation.Horizontal ? Orientation.Horizontal : Orientation.Vertical;
            TaskbarInfo.TaskbarSizeChanged += (sender, args) =>
            {
                TaskbarWidth = args.Size.Width;
                TaskbarHeight = args.Size.Height;
            };
            Options.MinVerticalSize.Height = TaskbarHeight;

            TaskbarEdge = TaskbarInfo.Edge;
            TaskbarOrientation = TaskbarInfo.Orientation == CSDeskBand.TaskbarOrientation.Horizontal ? Orientation.Horizontal : Orientation.Vertical;
            TaskbarWidth = TaskbarInfo.Size.Width;
            TaskbarHeight = TaskbarInfo.Size.Height;

            //Options.ContextMenuItems = ContextMenuItems;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CSDeskBandWpf_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                VirtualDesktop.CurrentChanged += VirtualDesktop_CurrentChanged;
                VirtualDesktop.Created += VirtualDesktop_Created;
                VirtualDesktop.Destroyed += VirtualDesktop_Destroyed;

                GenerateButtons();
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
        private void GenerateButtons()
        {
            try
            {
                pnlButtonContainer.Children.Clear();
                var desktops = VirtualDesktop.GetDesktops();
                int i = 1;
                foreach (var desktop in desktops)
                {
                    var button = new Button()
                    {
                        Content = i.ToString(),
                        Height = TaskbarHeight / rowCount,
                        Width = TaskbarHeight / rowCount,
                        BorderThickness = new Thickness(0),
                        Padding = new Thickness(0),

                    };
                    button.Tag = desktop.Id;
                    SetBackgroundBinding(button);
                    button.Click += Button_Click;
                    pnlButtonContainer.Children.Add(button);
                    Options.MinHorizontalSize.Width += (int)button.Width;
                    i++;
                }
                OnPropertyChanged("CurrentDesktopId");
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void SetBackgroundBinding(Button button)
        {
            //Background="{Binding Path=NurseOut,Converter={StaticResource int2color}}"
            Binding backgroundBinding = new Binding();
            backgroundBinding.Source = this;
            backgroundBinding.Path = new PropertyPath("CurrentDesktopId");
            backgroundBinding.ConverterParameter = button.Tag;
            backgroundBinding.Mode = BindingMode.TwoWay;
            backgroundBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            backgroundBinding.Converter = new SelectedButtonBackgroundByDesktopIdConverter();
            BindingOperations.SetBinding(button, Button.BackgroundProperty, backgroundBinding);

        }

        private void VirtualDesktop_Destroyed(object sender, VirtualDesktopDestroyEventArgs e)
        {
            Dispatcher.Invoke(GenerateButtons);
        }

        private void VirtualDesktop_Created(object sender, VirtualDesktop e)
        {
            Dispatcher.Invoke(GenerateButtons);
        }

        private void VirtualDesktop_CurrentChanged(object sender, VirtualDesktopChangedEventArgs e)
        {
            CurrentDesktopId = e.NewDesktop.Id;
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var currentButton = (Button)e.Source;
            var desktopId = (Guid)currentButton.Tag;
            var desktop = VirtualDesktop.FromId(desktopId);
            desktop.Switch();
        }
    }
}
